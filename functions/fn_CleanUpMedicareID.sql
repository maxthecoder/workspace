use Medmeasures2014_SOURCE
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_CleanUpMedicareID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_CleanUpMedicareID]
GO

create function fn_CleanUpMedicareID
(
	@MedicareID nvarchar(20)
)
returns nvarchar(20)
as
begin
	declare @return_value nvarchar(30);
	
	select	@return_value=
			case
				when len(@MedicareID)<9 then null
				when @MedicareID is null then null
				when @MedicareID like '0000%' then null
				when @MedicareID like '9999%' then null			
				when @MedicareID like '[A-Z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' then null
				when @MedicareID like '[A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' then null
				when @MedicareID like '[A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' then null
				when @MedicareID like '[A-Z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' then null
				when @MedicareID like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' then @MedicareID
				when @MedicareID like '[A-Z][A-Z][A-Z][0-9]%' then (replace((RIGHT(@MedicareID,(PATINDEX('%[0-9]',@MedicareID)-3))),'-',''))+(SUBSTRING(@MedicareID,0,(patindex('%[0-9]%',@MedicareID))))
				when @MedicareID like '[A-Z][A-Z][0-9]%' then replace(RIGHT(@MedicareID,(PATINDEX('%[0-9]',@MedicareID)-2)),'-','')+SUBSTRING(@MedicareID,0,(patindex('%[0-9]%',@MedicareID)))
				when @MedicareID like '[A-Z][0-9]%' then replace(RIGHT(@MedicareID,(PATINDEX('%[0-9]',@MedicareID)-1)),'-','')+SUBSTRING(@MedicareID,0,(patindex('%[0-9]%',@MedicareID)))
				when @MedicareID like '%[0-9][a-z]' then @MedicareID
				when @MedicareID like '%[0-9][a-z][a-z]' then @MedicareID
				when @MedicareID like '%[a-z]%' then @MedicareID
			end

return @return_value
end